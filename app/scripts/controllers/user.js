'use strict';

/**
 * @ngdoc function
 * @name sunApp.controller:MainCtrl
 * @description
 * # MainCtrl
 * Controller of the sunApp
 */
angular.module('webApp')
  .controller('UserCtrl', ['$scope', 'user', function($scope, user) {

  		$scope.us='';
  		$scope.userId = 2;
  		$scope.userInfo = {
  			'username' : null,
  			'email' : null,
  			'pwd' :null,
            'status':0,
            'type':0
  		};
        $scope.signUp = function(username, email, pwd) {
            $scope.userInfo.username = username;
            $scope.userInfo.email = email;
            $scope.userInfo.pwd = pwd;
            user.post($scope.userInfo, function(data) {
                    window.alert(data);
                }, function(error) {
                    console.log(error);
                });

        }
        $scope.login = function(email,pwd) {
            user.get(email, pwd, function(data) {
               if(data!=null) {
               	$location.path('/article');
               }
            }, function(error){
                console.log(error);
            });
        }
    	$scope.get=function(userId) { 		
    		users.get($scope.userId, function(userData) {
    			console.log(userData);
    			$scope.us = userData;
    		}, function(error) {
    			console.log(error);
    		});
    	}
    	$scope.list = function() {
    		users.all(function(userData) {
    			$scope.us = userData;
    		}, function(error) {
    			console.log(error);
    		});
    	}
    	$scope.delete = function(userId) {
    		users.delete(2, function(data) {
    			window.alert(data);
    		}, function(error) {
    			console.log(error);
    		});
    	}
    	$scope.update = function() {
    		users.put(253, $scope.userInfo, function(success) {
    			window.alert(success);
    		}, function(error) {
    			console.log(error)
    		});
    	}
    	$scope.createUser = function() {
    		users.post($scope.userInfo, 
    		function(data) {
    			window.alert(data)
    		}, function(error) {
    			console.log(error);
    		})
    	}
    	
    }]);

