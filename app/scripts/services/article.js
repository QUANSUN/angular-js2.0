'use strict';

/**
 * @ngdoc function
 * @name sunApp.controller:MainCtrl
 * @description
 * # MainCtrl
 * Controller of the sunApp
 */
angular.module('webApp')
	.factory("roles", ['$http', function($http) {
  		var url = "http://localhost:3000/api/Roles/";
      var obj = {
  			get:function(email, pwd, successCB, failCB) { 
   				 $http.get(url)
    				.success(function(data) {
              if(data.status=='success') {
                var flag = true;
                angular.forEach(data.data, function(value) {
                  if(flag) {
                  if(value.email==email&&value.pwd==pwd) {
                     successCB(value);
                     flag=false;
                  }
                }
                });
             } 
    				})
    				.error(function(error){
    					failCB(error);
    				});
  			},

  			delete:function(userId, successCB, failCB) {
  				$http.delete(url+userId)
  				.success(function() {
              successCB("OK")
  				})
  				.error(function(error) {
              failCB(error);
  				});
  			},
  			post:function(data, successCB, failCB) {
  				$http.post(url, data)
  				.success(function() {
              successCB("Success update");
  				})
  				.error(function(error) {
              failCB(error);
  				})
  			},
  			put:function(data, successCB, failCB) {
  				$http.put(url, data)
  				.success(function(data2) {
            console.log(data);
            console.log(data2);
              successCB("Success update");
  				})
  				.error(function(error) {
              failCB(error);
  				})
  			},
  			all:function(successCB, errorCB) {
  				$http.get(url)
  				.success(function(data) {
            console.log(data);
  					successCB(data.data);
  				})
  				.error(function(error) {
  					errorCB(error);
  				})
  			}
  		};
  		return obj;
  	}]);
