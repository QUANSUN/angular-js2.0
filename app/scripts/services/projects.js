'use strict';

/**
 * @ngdoc function
 * @name sunApp.controller:MainCtrl
 * @description
 * # MainCtrl
 * Controller of the sunApp
 */
angular.module('webApp')
	.factory("projects", ['$http', function($http) {
  		var url = "http://localhost:3000/api/Projects/";
      var obj = {
  			get:function(userId, successCB, failCB) { 
   				 $http.get(url+userId)
    				.success(function(data) {
              successCB(data.data);
    				})
    				.error(function(error){
    					failCB(error);
    				});
  			},

  			delete:function(userId, successCB, failCB) {
  				$http.delete(url+userId)
  				.success(function() {
              successCB("OK")
  				})
  				.error(function(error) {
              failCB(error);
  				});
  			},
  			post:function(data, successCB, failCB) {
  				$http.post(url, data)
  				.success(function() {
              successCB("Success update");
  				})
  				.error(function(error) {
              failCB(error);
  				})
  			},
  			put:function(data, successCB, failCB) {
  				$http.put(url, data)
  				.success(function(data2) {
            console.log(data);
            console.log(data2);
              successCB("Success update");
  				})
  				.error(function(error) {
              failCB(error);
  				})
  			},
  			all:function(successCB, errorCB) {
  				$http.get(url)
  				.success(function(data) {
            console.log(data);
  					successCB(data.data);
  				})
  				.error(function(error) {
  					errorCB(error);
  				})
  			}
  		};
  		return obj;
  	}]);
