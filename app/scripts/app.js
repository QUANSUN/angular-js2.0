'use strict';

/**
 * @ngdoc overview
 * @name sunApp
 * @description
 * # sunApp
 *
 * Main module of the application.
 */
angular
  .module('webApp', [
    'ngAnimate',
    'ngCookies',
    'ngResource',
    'ngRoute',
    'ngSanitize',
    'ngTouch'
  ])
  .config(function ($routeProvider) {
    $routeProvider
      .when('/reg', {
        templateUrl: 'views/reg.html',
        controller:  'UserCtrl'         
      })
      .when('/login', {
        templateUrl: 'views/login.html',
        controller:  'UserCtrl'         
      })
      .when('/article', {
        templateUrl: 'views/article.html',
        controller:  'ArticleCtrl'         
      })
      .otherwise({
        redirectTo: '/'
      });
  });
